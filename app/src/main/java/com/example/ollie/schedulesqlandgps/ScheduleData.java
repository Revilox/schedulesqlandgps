package com.example.ollie.schedulesqlandgps;

import java.io.Serializable;

public class ScheduleData implements Serializable {

    private String content;
    private Boolean completed;


	// constructors
	public ScheduleData() {}

    public ScheduleData(String content, Boolean completed) {
        this.content = content;
        this.completed = completed;
    }

    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public Boolean isCompleted() {
        return completed;
    }
    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }
}
