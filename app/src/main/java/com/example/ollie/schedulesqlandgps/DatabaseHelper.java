package com.example.ollie.schedulesqlandgps;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/*
*
* All database queries
*
*/

public class DatabaseHelper extends SQLiteOpenHelper {

    // singleton instance
    private static DatabaseHelper instance;

	// Logcat tag
	private static final String LOG = "DatabaseHelper";

	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "ReviloxDBManager";

	// Table Names
	private static final String KEY_ID = "id";
	private static final String KEY_CONTENT = "content";
	private static final String KEY_COMPLETED = "completed";
	private static final String TABLE_SCHEDULE = "schedule_table";

	// Table Create Statements
    // Activities table create statement
	private static final String CREATE_TABLE_SCHEDULE = "CREATE TABLE "
			+ TABLE_SCHEDULE + "("
            + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_CONTENT	+ " TEXT,"
            + KEY_COMPLETED	+ " TEXT"
            + ")";

	private DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

    public static DatabaseHelper getInstance(Context context){
        if(instance == null){
            instance = new DatabaseHelper(context);
        }
        return instance;
    }

	@Override
	public void onCreate(SQLiteDatabase db) {
        // creating required tables
		db.execSQL(CREATE_TABLE_SCHEDULE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// on upgrade drop older tables
		db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_SCHEDULE);

		// create new tables
		onCreate(db);
	}

    // remove a table
    public void clearTable(String table){
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("DELETE FROM "+ table);
    }

    public long createSchedule(ScheduleData schedule) {
        SQLiteDatabase db = this.getWritableDatabase();

        long article_id = 0;

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(KEY_CONTENT, schedule.getContent());
        values.put(KEY_COMPLETED, schedule.isCompleted());

        article_id = db.insertWithOnConflict(TABLE_SCHEDULE, null, values, SQLiteDatabase.CONFLICT_REPLACE);

        return article_id;
    }

    /**
     * getting all the schedules from db
     * */
    public List<ScheduleData> getAllSchedules(String table) {
        List<ScheduleData> schedules = new ArrayList();

        // 1. build the query
        String selectQuery = "SELECT  * FROM " + table;

        Log.e(LOG, selectQuery);

        // 2. get reference to writable DB
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        try {
            // looping through all rows and adding to list
            if (c.moveToFirst()) {
                do {
                    ScheduleData schedule = new ScheduleData();
                    schedule.setContent(c.getString(c.getColumnIndex(KEY_CONTENT)));
                    schedule.setCompleted(c.getInt(c.getColumnIndex(KEY_COMPLETED)) == 1);

                    // adding to schedule list
                    schedules.add(schedule);
                } while (c.moveToNext());
            }
        }
        catch (Exception ex) {
            System.out.println("Exception Occured : " + ex.toString());
            return null;
        }

        return schedules;
    }
}
