package com.example.ollie.schedulesqlandgps;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class SchedulelistActivity extends AppCompatActivity {

    public static final int REQUEST_CODE = 123;
    private ArrayList<ScheduleData> scheduleDataList;
    private ArrayAdapter<String> listViewAdapter;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedulelist);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_schedulelist, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_gps){
            Intent intent = new Intent(getApplicationContext(), Gps.class);
            startActivity(intent);
        }
        //logout action added to menu
        if (id == R.id.action_logout) {
            finish();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            return true;
            // Added action to add Icon -> AddSchedule
        }   else if(id == R.id.action_add) {
            Intent intent = new Intent(getApplicationContext(), AddScheduleActivity.class);


            startActivityForResult(intent, REQUEST_CODE);
            return true;
        } else if(id == R.id.action_refresh) {
            scheduleDataList = new ArrayList<>();
            scheduleDataList.addAll(DatabaseHelper.getInstance(this).getAllSchedules("schedule_table"));

            listView = (ListView) findViewById(R.id.list);
            listView.setAdapter(new CustomAdapter(this, scheduleDataList));

            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    //Method to handle dynamic insertion
    public void addItems(View v) {
        listViewAdapter.add("Clicked : ");
        listViewAdapter.notifyDataSetChanged();
    }

    //Checks to make sure if the result was okay or canceled depending on if back had been
    //clicked before or after the button
    //intent holds data
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_CODE) {
            if(resultCode == RESULT_OK) {
                Complete complete1 = (Complete) data.getSerializableExtra(AddScheduleActivity.COMPLETE_1);
                Toast.makeText(getApplicationContext(), "Result OK Content:" + complete1.content, Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "Result Canceled", Toast.LENGTH_SHORT).show();
            //pass data in form of object
            }
        }
    }
}
