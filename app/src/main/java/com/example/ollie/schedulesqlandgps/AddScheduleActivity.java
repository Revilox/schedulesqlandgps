package com.example.ollie.schedulesqlandgps;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class AddScheduleActivity extends AppCompatActivity {

    public static final String CONTENT = "content";
    public static final String DONE = "done";
    public static final String COMPLETE_1 = "complete1";
    private EditText contentEditText;
    private CheckBox completeCheckBox;
    private Button saveButton;
    private String content;
    private boolean complete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_schedule);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //connecting content boxes to code
        contentEditText = (EditText) findViewById(R.id.content_et);
        completeCheckBox = (CheckBox) findViewById(R.id.done_cb);
        saveButton = (Button) findViewById(R.id.save_btn);

        //check the save button on click
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //collect data
                content = contentEditText.getText().toString();
                //marks if the task has been finished
                complete = completeCheckBox.isChecked();

                SaveInDB();

                //have to use complete1 as complete is already in scope
                Complete complete1 = new Complete();
                complete1.content = content;
                complete1.done = complete;

                //passing data as intent
                Intent intent = new Intent();
                intent.putExtra(COMPLETE_1, complete1);

                setResult(RESULT_OK, intent);
                finish();
            }

        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_addschedule, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);

    }

    private void SaveInDB(){
        ScheduleData scheduleData;

        scheduleData = new ScheduleData(content, complete);

        DatabaseHelper.getInstance(LoginActivity.mainContext).createSchedule(scheduleData);
    }

}
