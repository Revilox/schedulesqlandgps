package com.example.ollie.schedulesqlandgps;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private Button button;
    private EditText usernameEditText;
    private EditText passwordEditText;
    public static Context mainContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mainContext = getApplicationContext();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // adding listener to check button click
        button = (Button) findViewById(R.id.login_button);
        button.setOnClickListener(this);
        usernameEditText = (EditText) findViewById(R.id.username_et);
        passwordEditText = (EditText) findViewById(R.id.password_et);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //validation check to make sure fields are filled
    @Override
    public void onClick(View v) {
        String username = usernameEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        boolean isError = false;
        // check to make sure the username field is not empty
        if (TextUtils.isEmpty(username)) {
            usernameEditText.setError(getString(R.string.username_is_required));
            isError = true;
        }
        //check to make sure username is also an email address
        if (!Patterns.EMAIL_ADDRESS.matcher(username).matches()) {
            passwordEditText.setError("This field will be your email");
        }
        // check to make sure password field is not empty
        if (TextUtils.isEmpty(password)) {
            passwordEditText.setError(getString(R.string.password_is_required));
            isError = true;
        }
        if (!isError) {
            login(username, password);
        }
    }

    //currently simulating times access to server for later
    //the login method & background operations
    private void login(String username, String password) {
        AsyncTask<String, Integer, Boolean> asyncTask = new AsyncTask<String, Integer, Boolean>() {
            @Override
            //do in background called and values extracted
            protected Boolean doInBackground(String... strings) {
                //val returned bool
                String username = strings[0];
                String password = strings[1];
                //using a loop for the login process count up to 100
                for (int i = 0; i < 100; i++){
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    //pass data of the progress that took place
                    publishProgress(i);
                }

                //the username and password set
                return username.equals("ollie@test.com") && password.equals("test");

            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);
                //logging data
                Log.d(TAG, "Progress" + values[0]);
                //show the value on the button
                button.setText(String.valueOf(values[0]));
            }

            //validation check right or wrong U&P or lack of U&P
            @Override
            protected void onPostExecute(Boolean logged) {
                super.onPostExecute(logged);
                //re allows button after the time even if good login or bad login
                button.setEnabled(true);
                if(logged) {
                    Toast.makeText(getApplicationContext(), "Login OK", Toast.LENGTH_SHORT).show();
                // once login is correct move to schedule list
                Intent intent = new Intent(getApplicationContext(),SchedulelistActivity.class);
                startActivity(intent);
                //stop back login
                finish();
            }
        }

            // after testing, implemented login button lock when async task starts
            //to stop multiple login
            @Override
        protected void onPreExecute() {
                super.onPreExecute();
                button.setEnabled(false);
            }
        };

        asyncTask.execute(username, password);


    }
}


