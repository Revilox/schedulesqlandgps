package com.example.ollie.schedulesqlandgps;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomAdapter extends BaseAdapter {

    Context context;
    ArrayList<ScheduleData> data;
    private static LayoutInflater inflater = null;

    public CustomAdapter(Context context, ArrayList<ScheduleData> data) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(R.layout.row, null);
        TextView content = (TextView) vi.findViewById(R.id.text1);
        TextView completed = (TextView) vi.findViewById(R.id.text2);
        content.setText("ToDo: " + data.get(position).getContent());
        completed.setText("Completed: " + data.get(position).isCompleted().toString());
        return vi;
    }
}